FROM alpine:latest

COPY rootfs/ /

RUN apk add --no-cache util-linux \
&& chmod +x /collector \
&& chmod +x /collect.sh \
&& sed -i 's/\r//g' /collect.sh \
&& apk add --no-cache iproute2 bash

ENTRYPOINT ["/collector", "run"]

